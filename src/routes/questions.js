var express = require('express');
var router = express.Router();
var questionsRepository = require('../repositories/questions_repository');

router.post('/', function(req, res) {
  questionsRepository.create(req.body).then(function(){
    res.sendStatus(201);
  }).catch(function (error) {
    if (error.name == 'ValidationError') {
      res.status(400).json({error: error.message});
    }
    else{
      console.error(error);
      res.sendStatus(500);
    }
  });
});


router.get('/:questionId', function(req, res) {
  res.status(200).send("Request to get question by id: "+ req.params.questionId);
});

router.get('/:questionId/answers', function(req, res) {
    res.status(200).send("Request to get all answer to question: " + req.params.questionId);
  });

router.get('/:questionId/answers/:answerId', function(req, res) {
    console.log(req.params);
  res.status(200).send("Request to get answer: " + req.params.answerId + " of question: "+ req.params.questionId);
});

module.exports = router
