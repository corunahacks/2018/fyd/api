var express = require('express');
var router = express.Router();

var questions  = require('./questions');
var votes  = require('./votes');

router.use('/questions', questions);
router.use('/votes', votes)

router.get('/', function(req, res) {
  console.log('Save & sound');

  res.status(200).send("Welcome to our restful API");
});

module.exports = router