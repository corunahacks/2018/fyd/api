var Question = require('../models/question');

var questionsRepository = function() {
  var create = function(questionOptions){
    return Question.create(questionOptions);
  };

  return { create: create }
}


module.exports = questionsRepository();
