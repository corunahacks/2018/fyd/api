var express = require('express');
var mongoose = require('mongoose');
var config = require('./config');

const { db } = config;
const connectionString = `mongodb://${db.host}:${db.port}/${db.name}`;
mongoose.connect(connectionString, { useNewUrlParser: true });


var server = express();
var bodyParser = require('body-parser');
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

var routes  = require('./routes/main');
server.use('/',routes);


var port = process.env.PORT || 8080;
server.listen(port);
console.log('Go to this port ' + port);

module.exports = server;
