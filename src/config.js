const env = process.env.NODE_ENV || 'dev';

const dev = {
 db: {
   host: 'localhost',
   port: 27017,
   name: 'fyd_dev'
 }
};

const test = {
 db: {
   host: 'localhost',
   port: 27017,
   name: 'fyd_test'
 }
};

const integration = {
  db: {
   host: 'mongo',
   port: 27017,
   name: 'fyd_test'
 }
}

const config = {
 dev,
 test,
 integration
};

module.exports = config[env];
