var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionSchema = new Schema(
  {
    name: { type: String, required: true },
    createdAt: { type: Date, default: Date.now },
  }
);

QuestionSchema.pre('save', next => {
  now = new Date();
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
});

module.exports = mongoose.model('question', QuestionSchema);
