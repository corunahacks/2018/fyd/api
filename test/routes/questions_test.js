var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../../src/server');
var should = chai.should();


chai.use(chaiHttp);

describe('Questions', function(){
  context('create question with a name in params', function(){
    var question_name = 'Question name';

    it('it should return status 201', function(done){
      chai.request(server)
          .post('/questions')
          .send({ name: question_name })
          .end(function(err, res){
            res.should.have.status(201);
            done();
          });
    });
  });

  context('create question without a name in params', function(){
    it('it should return status 400', function(done){
      chai.request(server)
          .post('/questions')
          .end(function(err, res){
            res.should.have.status(400);
            done();
          });
    });

    it('it should return error message', function(done){
      chai.request(server)
          .post('/questions')
          .end(function(err, res){
            res.body.error.should.not.be.empty;
            done();
          });
    });
  });
});
