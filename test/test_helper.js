const mongoose = require('mongoose');
const Cleaner = require('database-cleaner');
const dbCleaner = new Cleaner('mongodb');

afterEach(function(done){
  dbCleaner.clean(mongoose.connection.db, function(){
    done();
  });
});
