swagger: "2.0"
info:
  description: ""
  version: "1.0.0"
  title: "FYI Api spec"
basePath: "/"
paths:
  /questions:
    post:
      summary: "Add a new question to the system"
      operationId: "addQuestion"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Question object that needs to be added to the system"
        required: true
        schema:
          $ref: "#/definitions/Question"
      responses:
        405:
          description: "Invalid input"
  /questions/{questionId}:
    get:
      summary: "Find question by ID"
      description: "Returns a single question"
      operationId: "getQuestionById"
      produces:
      - "application/json"
      parameters:
      - name: "questionId"
        in: "path"
        description: "ID of question to return"
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "successful operation"
          schema:
            $ref: "#/definitions/Question"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Question not found"
    put:
      summary: "Update an existing question"
      description: ""
      operationId: "updateQuestion"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "questionId"
        in: "path"
        description: "ID of question to update"
        required: true
        type: "integer"
        format: "int64"
      - in: "body"
        name: "body"
        description: "Question object that needs to be added to the system"
        required: true
        schema:
          $ref: "#/definitions/Question"
      responses:
        400:
          description: "Invalid ID supplied"
        404:
          description: "Question not found"
        405:
          description: "Validation exception"
    delete:
      summary: "Deletes a question"
      description: ""
      operationId: "deleteQuestion"
      parameters:
      - name: "questionId"
        in: "path"
        description: "ID of question to delete"
        required: true
        type: "integer"
        format: "int64"
      responses:
        400:
          description: "Invalid ID supplied"
        404:
          description: "Question not found"
  /questions/{questionId}/answers:
    get:
      summary: "Find a question answers"
      description: "Returns an array with the answers of this question"
      operationId: "getAnswersByQuestionId"
      produces:
      - "application/json"
      parameters:
      - name: "questionId"
        in: "path"
        description: "ID of question to return answers"
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "successful operation"
          schema:
            type: array
            items:
              $ref: "#/definitions/Question"
        400:
          description: "Invalid ID supplied"
        404:
          description: "Question not found"
    post:
      summary: "Add a new answer to the provided question"
      operationId: "addAnswer"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "questionId"
        in: "path"
        description: "ID of question to return answers"
        required: true
        type: "integer"
        format: "int64"
      - in: "body"
        name: "body"
        description: "Answer object that needs to be added to the question"
        required: true
        schema:
          $ref: "#/definitions/Answer"
      responses:
        405:
          description: "Invalid input"
  /questions/{questionId}/answers/{answerId}:
    put:
      summary: "Updates one answer"
      operationId: "updateAnswer"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "questionId"
        in: "path"
        description: "ID of question"
        required: true
        type: "integer"
        format: "int64"
      - name: "answerId"
        in: "path"
        description: "ID of answer"
        required: true
        type: "integer"
        format: "int64"
      - in: "body"
        name: "body"
        description: "Answer object that needs to be updated"
        required: true
        schema:
          $ref: "#/definitions/Answer"
      responses:
        405:
          description: "Invalid input"
    delete:
      summary: "Deletes an answer"
      description: ""
      operationId: "deleteAnswer"
      parameters:
      - name: "questionId"
        in: "path"
        description: "ID of question"
        required: true
        type: "integer"
        format: "int64"
      - name: "answerId"
        in: "path"
        description: "ID of answer"
        required: true
        type: "integer"
        format: "int64"
      responses:
        400:
          description: "Invalid ID supplied"
        404:
          description: "Answer not found"
definitions:
  Question:
    type: "object"
    required:
    - "name"
    properties:
      id:
        type: "integer"
        format: "int64"
      name:
        type: "string"
        example: "Dinner schedule"
  Answer:
    type: "object"
    required:
    - "name"
    properties:
      id:
        type: "integer"
        format: "int64"
      questionId:
        type: "integer"
        format: "int64"
      name:
        type: "string"
        example: "Saturday night"